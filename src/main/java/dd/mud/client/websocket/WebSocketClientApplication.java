package dd.mud.client.websocket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.kafka.KafkaAutoConfiguration;

@SpringBootApplication(exclude = KafkaAutoConfiguration.class)
public class WebSocketClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebSocketClientApplication.class, args);
	}

}
