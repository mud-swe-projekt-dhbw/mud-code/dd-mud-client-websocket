package dd.mud.client.websocket.controller.service.answer;

import dd.mud.client.websocket.boundary.model.kafka.CompactGame;
import dd.mud.client.websocket.boundary.websocket.sender.DMPlayerTopicSender;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class DMGameUpdateService {

    private final DMPlayerTopicSender dmPlayerTopicSender;

    /**
     * Methods forwards the incoming game object to the corresponding Dungeon Master by ID
     * @param compactGame compact game object from Kafka listener
     */
    public void sendCurrentGameToClient (CompactGame compactGame) {
        this.dmPlayerTopicSender.sendToPlayerDMWebsocket(compactGame.getDungeonMasterId(), compactGame.getGameId(), compactGame);
    }
}
