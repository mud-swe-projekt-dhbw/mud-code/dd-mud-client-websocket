package dd.mud.client.websocket.controller.service.forward;

import dd.mud.client.websocket.boundary.kafka.kafkaproducer.SendToGameServiceProducer;
import dd.mud.client.websocket.boundary.model.kafka.MudKafkaMessage;
import dd.mud.client.websocket.boundary.model.websocket.SimpleTextMessage;
import dd.mud.client.websocket.controller.players.PlayerWaitingStatus;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class GameMessageForwardingService {

    private final PlayerWaitingStatus playerWaitingStatus;
    private final SendToGameServiceProducer sendToGameServiceProducer;

    /**
     * Forward message to game service
     * @param playerId player id who send the message
     * @param textMessage message object
     */
    public void forwardGameMessage(Long playerId, Long gameId, SimpleTextMessage textMessage) {
        if (this.playerWaitingStatus.checkAndLock(playerId).equals(Boolean.TRUE)) {
            this.sendToGameServiceProducer.sendToGameService(createMudKafkaMessage(playerId, gameId, textMessage));
        }
    }

    private MudKafkaMessage createMudKafkaMessage (Long playerId, Long gameId, SimpleTextMessage textMessage) {
        return MudKafkaMessage.builder()
                .id(playerId)
                .gameId(gameId)
                .message(textMessage.getMessage())
                .build();
    }
}
