package dd.mud.client.websocket.controller.service.answer;

import dd.mud.client.websocket.boundary.model.kafka.ChatServiceAnswer;
import dd.mud.client.websocket.boundary.model.websocket.ChatMessage;
import dd.mud.client.websocket.boundary.websocket.sender.ChatPlayerTopicSender;
import dd.mud.client.websocket.controller.players.PlayerWaitingStatus;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class ChatServiceAnswerService {

    private final PlayerWaitingStatus playerWaitingStatus;
    private final ChatPlayerTopicSender chatPlayerTopicSender;

    /**
     * Method for processing chat service answer.
     * @param chatServiceAnswer chat service answer object coming from Kafka listener
     */
    public void processChatServiceAnswer (ChatServiceAnswer chatServiceAnswer) {
        this.playerWaitingStatus.changeStatusOfPlayer(chatServiceAnswer.getSenderId(), Boolean.TRUE);
        ChatMessage chatMessage = this.createChatMessage(chatServiceAnswer);
        chatServiceAnswer.getRecipients().forEach(
                recipient -> this.chatPlayerTopicSender.sendToPlayerChatWebsocketTopic(recipient, chatMessage)
        );
        this.chatPlayerTopicSender.sendToPlayerChatWebsocketTopic(chatServiceAnswer.getSenderId(), chatMessage);
    }

    private ChatMessage createChatMessage (ChatServiceAnswer chatServiceAnswer) {
        return ChatMessage.builder()
                .name(chatServiceAnswer.getSenderName())
                .message(chatServiceAnswer.getMessage())
                .build();
    }
}
