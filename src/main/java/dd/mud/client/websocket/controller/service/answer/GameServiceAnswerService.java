package dd.mud.client.websocket.controller.service.answer;

import dd.mud.client.websocket.boundary.model.kafka.MudKafkaMessage;
import dd.mud.client.websocket.boundary.model.websocket.SimpleTextMessage;
import dd.mud.client.websocket.boundary.websocket.sender.GamePlayerTopicSender;
import dd.mud.client.websocket.controller.players.PlayerWaitingStatus;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class GameServiceAnswerService {

    private final PlayerWaitingStatus playerWaitingStatus;
    private final GamePlayerTopicSender gamePlayerTopicSender;

    /**
     * Method for processing the incoming game service answer. Just forwarding.
     * @param mudKafkaMessage message object from Kafka listener
     */
    public void processGameServiceAnswer (MudKafkaMessage mudKafkaMessage) {
        this.playerWaitingStatus.changeStatusOfPlayer(mudKafkaMessage.getId(), Boolean.TRUE);
        this.gamePlayerTopicSender.sendToPlayerGameWebsocket(mudKafkaMessage.getId(), mudKafkaMessage.getGameId(), SimpleTextMessage.builder()
            .message(mudKafkaMessage.getMessage()).build()
        );
    }
}
