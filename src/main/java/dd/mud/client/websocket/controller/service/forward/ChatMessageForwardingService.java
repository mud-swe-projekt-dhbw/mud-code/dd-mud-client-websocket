package dd.mud.client.websocket.controller.service.forward;

import dd.mud.client.websocket.boundary.kafka.kafkaproducer.SendToChatServiceProducer;
import dd.mud.client.websocket.boundary.model.kafka.MudKafkaMessage;
import dd.mud.client.websocket.boundary.model.websocket.ChatMessage;
import dd.mud.client.websocket.controller.players.PlayerWaitingStatus;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class ChatMessageForwardingService {

    private final PlayerWaitingStatus playerWaitingStatus;
    private final SendToChatServiceProducer chatServiceProducer;

    /**
     * Forward the chat message to the chat service
     * @param playerId player id sending message
     * @param chatMessage chat massage object
     */
    public void forwardChatMessage (Long playerId, Long gameId, ChatMessage chatMessage) {
        if (this.playerWaitingStatus.checkAndLock(playerId).equals(Boolean.TRUE)) {
            this.chatServiceProducer.sendToChatService(this.constructMudKafkaMessage(playerId, gameId, chatMessage));
        }
    }

    private MudKafkaMessage constructMudKafkaMessage (Long playerId, Long gameId, ChatMessage chatMessage) {
        return MudKafkaMessage.builder()
                .id(playerId)
                .gameId(gameId)
                .message(chatMessage.getName() + ":" + chatMessage.getMessage())
                .build();
    }
}
