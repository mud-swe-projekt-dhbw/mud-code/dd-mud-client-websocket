package dd.mud.client.websocket.controller.players;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * Component holding information if player is waiting for an answer from a service
 * Realized with a hash map that maps the player id to a boolean
 * boolean: False if waiting for answer, True otherwise
 * Crucial information on weather or not a player is allowed to submit another message.
 */
@Component
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class PlayerWaitingStatus {

    @Getter
    public Map<Long, Boolean> playerWaitingMap = new HashMap<>();

    /**
     * Check if player is allowed to send message and lock if he/she is
     * @param playerId player id
     * @return status if sending is possible
     */
    public Boolean checkAndLock (Long playerId) {
        Boolean status = this.playerWaitingMap.get(playerId);
        status = checkIfPlayerExists(playerId, status);
        if (status.equals(Boolean.TRUE)) lockPlayer(playerId);
        return status;
    }

    /**
     * Change player status manually
     * @param playerId player Id
     * @param newStatus status to set
     */
    public void changeStatusOfPlayer (Long playerId, Boolean newStatus) {
        this.playerWaitingMap.replace(playerId, newStatus);
    }

    /**
     * remove player from map
     * @param playerId player Id
     */
    public void removePlayer (Long playerId) {
        this.playerWaitingMap.remove(playerId);
    }

    private void registerNewPlayer (Long playerId) {
        this.playerWaitingMap.put(playerId, Boolean.TRUE);
    }

    private Boolean checkIfPlayerExists(Long playerId, Boolean status) {
        if (status == null) {
            this.registerNewPlayer(playerId);
            status = Boolean.TRUE;
        }
        return status;
    }

    private void lockPlayer (Long playerId) {
        this.changeStatusOfPlayer(playerId, Boolean.FALSE);
    }
}
