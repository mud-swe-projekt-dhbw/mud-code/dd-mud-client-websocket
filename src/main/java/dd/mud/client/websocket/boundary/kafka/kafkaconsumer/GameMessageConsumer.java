package dd.mud.client.websocket.boundary.kafka.kafkaconsumer;

import dd.mud.client.websocket.boundary.model.kafka.MudKafkaMessage;
import dd.mud.client.websocket.controller.service.answer.GameServiceAnswerService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class GameMessageConsumer {

    private final GameServiceAnswerService gameServiceAnswerService;

    /**
     * Method listens to kafka topic for receiving messages from the game service.
     * @param gameMessage game service message object
     */
    @KafkaListener(topics = "gameAnswer.t",
            groupId = "websocketServiceGroup",
            containerFactory = "gameServiceContainerFactory")
    public void receiveGameServiceAnswer (MudKafkaMessage gameMessage) {
        System.out.println("Incoming from game");
        this.gameServiceAnswerService.processGameServiceAnswer(gameMessage);
    }
}
