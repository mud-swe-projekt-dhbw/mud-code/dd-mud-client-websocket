package dd.mud.client.websocket.boundary.kafka.kafkaconsumer;

import dd.mud.client.websocket.boundary.model.kafka.ChatServiceAnswer;
import dd.mud.client.websocket.controller.service.answer.ChatServiceAnswerService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class ChatMessageConsumer {

    private final ChatServiceAnswerService chatServiceAnswerService;

    /**
     * Method listens to kafka topic for receiving messages from the chat service.
     * @param chatServiceAnswer chat service answer object
     */
    @KafkaListener(topics = "chatAnswer.t",
            groupId = "websocketServiceGroup",
            containerFactory = "chatServiceContainerFactory")
    public void receiveChatServiceAnswer (ChatServiceAnswer chatServiceAnswer) {
        System.out.println("incoming from chat");
        this.chatServiceAnswerService.processChatServiceAnswer(chatServiceAnswer);
    }
}
