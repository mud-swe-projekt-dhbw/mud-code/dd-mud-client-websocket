package dd.mud.client.websocket.boundary.websocket.sender;

import dd.mud.client.websocket.boundary.model.websocket.ChatMessage;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class ChatPlayerTopicSender {

    private final SimpMessagingTemplate template;

    /**
     * Sends chat message to given player by id
     * @param playerId player's id
     * @param chatMessage message object
     */
    public void sendToPlayerChatWebsocketTopic (Long playerId, ChatMessage chatMessage) {
        this.template.convertAndSend("/topic/chat/player/" + playerId, chatMessage);
    }
}
