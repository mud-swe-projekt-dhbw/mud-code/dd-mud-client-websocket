package dd.mud.client.websocket.boundary.websocket.receiver;

import dd.mud.client.websocket.boundary.model.websocket.SimpleTextMessage;
import dd.mud.client.websocket.controller.service.forward.GameMessageForwardingService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.stereotype.Controller;

@Controller
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class GameSocketApi {

    private final GameMessageForwardingService gameMessageForwardingService;

    /**
     * Entry point for websocket game messages
     * @param playerId id of player who send the message
     * @param simpleTextMessage text message object
     */
    @MessageMapping("/game/{game-id}/player/{player-id}/input")
    public void receiveGameMessage (@DestinationVariable(value = "game-id") Long gameId,
                                    @DestinationVariable(value = "player-id") Long playerId,
                                    SimpleTextMessage simpleTextMessage) {
        System.out.println("WS incoming game");
        this.gameMessageForwardingService.forwardGameMessage(playerId, gameId, simpleTextMessage);
    }
}
