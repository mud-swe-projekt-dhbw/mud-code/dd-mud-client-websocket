package dd.mud.client.websocket.boundary.kafka.kafkaconsumer;

import dd.mud.client.websocket.boundary.model.kafka.CompactGame;
import dd.mud.client.websocket.controller.service.answer.DMGameUpdateService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class DungeonMasterConsumer {

    private final DMGameUpdateService dmGameUpdateService;

    /**
     * Method listens to kafka topic for receiving messages from the dungeon master service.
     * @param compactGame current updated game object
     */
    @KafkaListener(topics = "dungeonMasterAnswer.t",
            groupId = "websocketServiceGroup",
            containerFactory = "dmServiceContainerFactory")
    public void getGameUpdateFromDMService (CompactGame compactGame) {
        System.out.println("Got compact game from DMService");
        this.dmGameUpdateService.sendCurrentGameToClient(compactGame);
    }
}
