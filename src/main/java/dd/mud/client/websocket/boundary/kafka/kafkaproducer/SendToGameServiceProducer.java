package dd.mud.client.websocket.boundary.kafka.kafkaproducer;

import dd.mud.client.websocket.boundary.model.kafka.MudKafkaMessage;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class SendToGameServiceProducer {

    private static final String GAME_SERVICE_TOPIC = "gameGameMessage.t";

    private final KafkaTemplate<String, MudKafkaMessage> kafkaTemplate;

    /**
     * Method for sending messages to the game service
     * @param mudKafkaMessage message object for game service
     */
    public void sendToGameService (MudKafkaMessage mudKafkaMessage) {
        System.out.println("Send to game");
        this.kafkaTemplate.send(GAME_SERVICE_TOPIC, mudKafkaMessage);
    }
}
