package dd.mud.client.websocket.boundary.websocket.sender;

import dd.mud.client.websocket.boundary.model.websocket.SimpleTextMessage;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class GamePlayerTopicSender {

    private final SimpMessagingTemplate template;

    /**
     * Sends game message to player by id
     * @param playerId player's id
     * @param textMessage message object
     */
    public void sendToPlayerGameWebsocket (Long playerId, Long gameId, SimpleTextMessage textMessage) {
        System.out.println("send to player id " + playerId + " " +textMessage);
        this.template.convertAndSend("/topic/game/player/" + playerId + "/game/" + gameId, textMessage);
    }
}
