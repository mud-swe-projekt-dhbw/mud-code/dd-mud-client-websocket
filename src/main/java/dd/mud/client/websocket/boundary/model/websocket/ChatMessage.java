package dd.mud.client.websocket.boundary.model.websocket;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Simple chat message coming from the client. Has a name and the message itself.
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor
@Data
@Builder
public class ChatMessage {
    String name;
    String message;
}
