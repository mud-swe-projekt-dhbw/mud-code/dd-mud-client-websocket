package dd.mud.client.websocket.boundary.kafka.kafkaproducer;

import dd.mud.client.websocket.boundary.model.kafka.MudKafkaMessage;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class SendToChatServiceProducer {

    private static final String CHAT_SERVICE_TOPIC = "chatChatMessage.t";

    private final KafkaTemplate<String, MudKafkaMessage> kafkaTemplate;

    /**
     * Method for sending messages to the chat service
     * @param mudKafkaMessage message object for the chat service
     */
    public void sendToChatService (MudKafkaMessage mudKafkaMessage) {
        System.out.println("Send to Chat:  " + mudKafkaMessage.getId() + " " + mudKafkaMessage.getGameId() + " " + mudKafkaMessage.getMessage());

        this.kafkaTemplate.send(CHAT_SERVICE_TOPIC, mudKafkaMessage);
    }
}
