package dd.mud.client.websocket.boundary.model.kafka;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ChatServiceAnswer {
    Long senderId;
    String senderName;
    List<Long> recipients;
    String message;
}
