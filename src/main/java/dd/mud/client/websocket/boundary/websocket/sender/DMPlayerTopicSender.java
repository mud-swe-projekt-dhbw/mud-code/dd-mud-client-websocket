package dd.mud.client.websocket.boundary.websocket.sender;

import dd.mud.client.websocket.boundary.model.kafka.CompactGame;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class DMPlayerTopicSender {

    private final SimpMessagingTemplate template;

    /**
     * Sends dungeon master compact game to given player by id
     * @param playerId player's id
     * @param compactGame game object
     */
    public void sendToPlayerDMWebsocket (Long playerId, Long gameId, CompactGame compactGame) {
        System.out.println("send out to player compact game: " + playerId);
        this.template.convertAndSend("topic/dungeonmaster/player/" + playerId + "/game/" + gameId, compactGame);
    }
}
