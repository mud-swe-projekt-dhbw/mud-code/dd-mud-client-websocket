package dd.mud.client.websocket.boundary.websocket.receiver;

import dd.mud.client.websocket.boundary.model.websocket.ChatMessage;
import dd.mud.client.websocket.controller.service.forward.ChatMessageForwardingService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.stereotype.Controller;

@Controller
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class ChatSocketApi {

    private final ChatMessageForwardingService chatMessageForwardingService;

    /**
     * Entry point for websocket messages for the chat service
     * @param playerId id of sending player
     * @param chatMessage message object vom web client
     */
    @MessageMapping("/chat/game/{game-id}/player/{player-id}/input")
    public void receiveChatMessageFromPlayer (@DestinationVariable(value = "game-id") Long gameId,
                                              @DestinationVariable(value = "player-id") Long playerId,
                                              ChatMessage chatMessage) {
        this.chatMessageForwardingService.forwardChatMessage(playerId, gameId, chatMessage);
    }
}
