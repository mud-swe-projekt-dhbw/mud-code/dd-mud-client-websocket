package dd.mud.client.websocket.boundary.model.kafka;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Simple multipurpose object. Gets send to chat and game service
 * and received by the game service.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MudKafkaMessage {
    Long id;
    String message;
    Long gameId;
}
