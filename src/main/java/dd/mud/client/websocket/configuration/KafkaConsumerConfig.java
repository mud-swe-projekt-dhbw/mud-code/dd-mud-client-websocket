package dd.mud.client.websocket.configuration;

import dd.mud.client.websocket.boundary.model.kafka.ChatServiceAnswer;
import dd.mud.client.websocket.boundary.model.kafka.CompactGame;
import dd.mud.client.websocket.boundary.model.kafka.MudKafkaMessage;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.config.KafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.listener.ConcurrentMessageListenerContainer;
import org.springframework.kafka.support.serializer.JsonDeserializer;

import java.util.HashMap;
import java.util.Map;

@Configuration
@EnableKafka
public class KafkaConsumerConfig {

    @Value("${spring.kafka.bootstrap-servers}")
    private String bootstrapServers;

    @Bean
    public Map<String, Object> consumerConfigs() {
        Map<String, Object> props = new HashMap<>();
        // list of host:port pairs used for establishing the initial connections to the Kafka cluster
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG,
                bootstrapServers);
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,
                StringDeserializer.class);
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,
                JsonDeserializer.class);
        return props;
    }

    // first listener

    @Bean
    public ConsumerFactory<String, MudKafkaMessage> consumerFactoryGameService() {
        return new DefaultKafkaConsumerFactory<>(consumerConfigs(), new StringDeserializer(),
                new JsonDeserializer<>(MudKafkaMessage.class).ignoreTypeHeaders());
    }

    @Bean(name = "gameServiceContainerFactory")
    public KafkaListenerContainerFactory<ConcurrentMessageListenerContainer<String, MudKafkaMessage>>
        kafkaListenerContainerFactoryGameService() {
        ConcurrentKafkaListenerContainerFactory<String, MudKafkaMessage> factory =
                new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFactoryGameService());

        return factory;
    }

    // second listener

    @Bean
    public ConsumerFactory<String, ChatServiceAnswer> consumerFactoryChatService() {
        return new DefaultKafkaConsumerFactory<>(consumerConfigs(), new StringDeserializer(),
                new JsonDeserializer<>(ChatServiceAnswer.class).ignoreTypeHeaders());
    }

    @Bean(name = "chatServiceContainerFactory")
    public KafkaListenerContainerFactory<ConcurrentMessageListenerContainer<String, ChatServiceAnswer>>
        kafkaListenerContainerFactoryChatService() {
        ConcurrentKafkaListenerContainerFactory<String, ChatServiceAnswer> factory =
                new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFactoryChatService());

        return factory;
    }

    // third listener

    @Bean
    public ConsumerFactory<String, CompactGame> consumerFactoryDMService() {
        return new DefaultKafkaConsumerFactory<>(consumerConfigs(), new StringDeserializer(),
                new JsonDeserializer<>(CompactGame.class).ignoreTypeHeaders());
    }

    @Bean(name = "dmServiceContainerFactory")
    public KafkaListenerContainerFactory<ConcurrentMessageListenerContainer<String, CompactGame>>
    kafkaListenerContainerFactoryDMService() {
        ConcurrentKafkaListenerContainerFactory<String, CompactGame> factory =
                new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFactoryDMService());

        return factory;
    }
}