$("#resetPasswordForm").submit(function(e){
    e.preventDefault();
    resetPassword();
});

function resetPassword () {
    const urlParams = new URLSearchParams(window.location.search);
    const mailHash = urlParams.get('mailHash');

    const newPassword = $("#newPasswordInput");
    const newPasswordWDH = $("#newPasswordInputWDH");
    const status = $("#status");

    if (newPasswordWDH.val() === newPassword.val()) {
        const passwordObject = {
            "password": newPassword.val()
        };

        $.ajax({
            type: "PUT",
            url: "https://dd01.dv-xlab.de:49152/login/reset-password?mailHash=" + mailHash,
            contentType: "application/json",
            data: JSON.stringify(passwordObject),
            success: function () {
                status.text("Password erfolgreich zurückgesetzt!");
                clearFields(newPassword, newPasswordWDH);
            },
            error: function () {
                status.text("Beim Zurücksetzen ist ein Fehler passiert. Versuche es später noch einmal.");
                clearFields(newPassword, newPasswordWDH);
            }
        });
    } else {
        status.text("Passwörter stimmen nicht überein!");
        clearFields(newPassword, newPasswordWDH);
    }
}

function clearFields (newPassword, newPasswordWDH) {
    newPassword.val("");
    newPasswordWDH.val("");
}