function fromLoginToChooseGame () {
    $("#loginWrapper").attr("style", "display: none");
    $("#gameChooseWrapper").attr("style", "display: block");
}

function fromChooseGameToLogin () {
    $("#gameChooseWrapper").attr("style", "display: none");
    $("#loginWrapper").attr("style", "display: block");
}

function fromChooseGameToGameConfig() {
    $("#gameChooseWrapper").attr("style", "display: none");
    $("#gameConfigWrapper").attr("style", "display: block");
}

function fromChooseGameToCharacterConfig() {
    $("#gameChooseWrapper").attr("style", "display: none");
    $("#characterConfigWrapper").attr("style", "display: block");
}

function fromChooseGameToGame() {
    $("#gameChooseWrapper").attr("style", "display: none");
    $("#gameBoardWrapper").attr("style", "display: block");
}

function fromChooseGameToDM() {
    $("#gameChooseWrapper").attr("style", "display: none");
    $("#dungeonMasterWrapper").attr("style", "display: block");
}

function fromDMToChooseGame() {
    $("#dungeonMasterWrapper").attr("style", "display: none");
    $("#gameChooseWrapper").attr("style", "display: block");
}

function fromCharacterConfigToGame() {
    $("#characterConfigWrapper").attr("style", "display: none");
    $("#gameBoardWrapper").attr("style", "display: block");
}

function fromCharacterConfigToChooseGame() {
    $("#characterConfigWrapper").attr("style", "display: none");
    $("#gameChooseWrapper").attr("style", "display: block");
}

function fromGameToChooseGame() {
    $("#gameBoardWrapper").attr("style", "display: none");
    $("#gameChooseWrapper").attr("style", "display: block");
}

function fromGameConfigToChooseGame() {
    $("#gameConfigWrapper").attr("style", "display: none");
    $("#gameChooseWrapper").attr("style", "display: block");
}