/**
 * Method for sign-up player
 */
function signUp() {
    const password = $("#pswrdSU");
    if ($("#legalCheck").is(":checked")) {
        if (password.val() === $("#pswrdSURepeat").val()) {
            const signUpBody = {
                "mail": $("#email").val().toString().trim(),
                "username": $("#usrNameSU").val().toString(),
                "password": password.val().toString()
            };
            $.ajax({
                url: 'https://dd01.dv-xlab.de:49152/login/sign-up',
                type: 'POST',
                contentType: 'application/json',
                data: JSON.stringify(signUpBody),
                success: function () {
                    $("#signUpFormStatus").text("Anmeldung erfolgreich!\n" +
                        "Bestätigen Sie die Anmeldung über die angegebene E-Mail-Adresse.");
                },
                error: function (response) {
                    const status = $("#signUpFormStatus");
                    switch (response.status) {
                        case 400:
                            status.text("Benutzername oder E-Mail ist schon vergeben!");
                            break;
                        case 422:
                            let statusHtml = status.text("\nE-Mail-Adresse/Benutzername ungültig!\nUnzulässige Symbole im Benutzernamen: „@“, „:“, „ “.\nUnzulässige Benutzernamen: „DM“, „dm“, „Dm“, „dM“, „“.")
                            statusHtml.html(statusHtml.html().replace(/\n/g, '<br/>'));
                            break;
                        case 500:
                            status.text("Ein unerwarteter Fehler ist aufgetreten. Versuchen Sie es später noch einmal.");
                            break;
                    }

                }
            });
            cleanUpSignUp();
        } else {
            window.alert("Passwörter stimmen nicht überein.\nKorrigieren sie Ihre Eingabe.")
        }
    } else {
        window.alert("Bitte bestätigen sie die Weiterverarbeitung Ihrer E-Mail-Adresse.");
    }
}

function cleanLogIn() {
    $("#usrNameEMail").val("");
    $("#pswrd").val("");
}

/**
 * Login Method
 * @returns If successful returns corresponding player id
 */
function login() {
    const loginBody = {
        "mailOrUsername": $("#usrNameEMail").val().toString(),
        "password": $("#pswrd").val().toString()
    };
    $.ajax({
        url: 'https://dd01.dv-xlab.de:49152/login/check-credentials',
        type: 'POST',
        contentType: 'application/json',
        dataType: 'json',
        data: JSON.stringify(loginBody),
        success: function (response) {
            playerId = response.playerId;
            username = response.userName;
            cleanLogIn();
            continueToChooseGame();
            getGames(playerId);
        },
        error: function () {
            $("#logInStatus").text("Falscher Benutzer oder Passwort.");
            cleanLogIn();
        }
    });
}

/**
 * Method called when password is not remembered
 */
function forgotPassword() {
    const email = $("#passwordForgottonEmail");
    const passwordForgottenObject = {
        "mail": email.val().toString()
    }
    $.ajax({
        url: 'https://dd01.dv-xlab.de:49152/login/password-forgotten',
        type: 'PUT',
        contentType: 'application/json',
        dataType: 'json',
        data: JSON.stringify(passwordForgottenObject)
    });
    email.val("");
    window.alert("Falls die E-Mail exsistiert, haben Sie eine Rücksetz-E-Mail bekommen.");
}

/**
 * Use state change methods to go to game choose after successful log in
 */
function continueToChooseGame() {
    fromLoginToChooseGame();
}

/**
 * Cleans up log-in form
 */
function cleanUpSignUp() {
    $("#usrNameSU").val("");
    $("#pswrdSU").val("");
    $("#pswrdSURepeat").val("");
    $("#email").val("");
}