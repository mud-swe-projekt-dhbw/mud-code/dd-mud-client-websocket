function createNewGame () {
    if (!gameConfigFormIsValid()) return;
    const gameObject = createGameObject();
    $.ajax({
        url: 'https://dd01.dv-xlab.de:49153/games/new',
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(gameObject),
        success: function () {
            cleanUpGameConfigForm();
            returnToChooseGame("gameConfig")
        },
        error: function (response) {
            switch (response.status) {
                case 400:
                    window.alert("Oh. Dein Spiel-Objekt scheint fehlerhaft zu sein.\nPrüfe deine Eingaben");
                    break;
                case 500:
                    window.alert("Ein unerwarteter Fehler ist aufgetreten. Versuche es später noch einmal.");
                    break;
            }

        }
    });
}

/**
 * Function takes values from the HTML and creates game object
 * @return game object
 */
function createGameObject () {
    // Accumulate data from game config form;
    const gameDTO = createGameInfoDTO();
    const classes = getClassInformation();
    const races = getRaceInformation();
    const items = getItemInformation();
    const rooms = getRoomInformation();
    const itemRooms = getItemRoomInformation();

    // Create final object
    const gameObject = {
        "gameDTO": gameDTO,
        "classDTOs": getClassesAndRacesName(classes, "className"),
        "raceDTOs": getClassesAndRacesName(races, "raceName"),
        "itemDTOs": items,
        "roomDTOs": rooms,
        "abilityDTOs": extractAllAbilities(classes, races),
        "itemRoomDTOs": itemRooms,
        "classAbilityDTOs": getAbilityTuple(classes, "className"),
        "raceAbilityDTOs": getAbilityTuple(races, "raceName")
    };
    return gameObject;
}

/**
 * Return game meta data object
 * @return {{gameId: string, gameName: string | jQuery, userId: string}}
 */
function createGameInfoDTO() {
    const gameName = $("#gameName");
    return {
            "gameName": gameName.val().toString(),
            "userId": String(playerId),
            "gameId": String(0) // Dummy number with no meaning
    };
}

function getClassInformation() {
    let classes = [];
    let classNames = [];
    let classAbilities = [];
    $(".classCreate").each(function () {
         const className = $(this).val().toString();
         if (className !== "") classNames.push(className);
    });
    $(".classCreateAbility").each(function () {
        const classAbil = $(this).val().toString();
        if (classAbil !== "") classAbilities.push(parseCommata(classAbil));
    });

    for (let i = 0; i < classNames.length; i++) {
        classes.push({
            name: classNames[i],
            abil: classAbilities[i]
        });
    }

    return classes;
}

function getRaceInformation() {
    let races = [];
    let raceNames = [];
    let raceAbilities = [];
    $(".createRace").each(function () {
        const className = $(this).val().toString();
        if (className !== "") raceNames.push(className);
    });
    $(".createRaceAbility").each(function () {
        const raceAbil = $(this).val().toString();
        if (raceAbil !== "") raceAbilities.push(parseCommata(raceAbil));
    });

    for (let i = 0; i < raceNames.length; i++) {
        races.push({
            name: raceNames[i],
            abil: raceAbilities[i]
        });
    }

    return races;
}

function getItemInformation() {
    let items = [];
    let itemTypes = [];
    let itemNames = [];
    let itemValues = [];
    $(".createItem").each(function () {
        const itemType = $(this).val().toString();
        if (itemType !== "") itemTypes.push(itemType);
    });
    $(".createItemName").each(function () {
        const itemName = $(this).val().toString();
        if (itemName !== "") itemNames.push(itemName);
    });
    $(".createItemValue").each(function () {
        const itemValue = $(this).val().toString();
        if (itemValue !== "") itemValues.push(itemValue);
    });

    for (let i = 0; i < itemTypes.length; i++) {
        items.push({
            itemType: itemTypes[i],
            itemName: itemNames[i],
            itemValue: itemValues[i]
        });
    }
    return items;
}

function getRoomInformation() {
    let rooms = [];
    let roomNames = [];
    let xs = [];
    let ys = [];
    let roomDescriptions = [];
    $(".createRoom").each(function () {
        const roomName = $(this).val().toString();
        if (roomName !== "") roomNames.push(roomName);
    });
    $(".createRoomX").each(function () {
        const roomX = $(this).val().toString();
        if (roomX !== "") xs.push(roomX);
    });
    $(".createRoomY").each(function () {
        const roomY = $(this).val().toString();
        if (roomY !== "") ys.push(roomY);
    });
    $(".createRoomDescription").each(function () {
        const roomDesc = $(this).val().toString();
        if (roomDesc !== "") roomDescriptions.push(roomDesc);
    });
    for (let i = 0; i < roomNames.length; i++) {
        rooms.push({
            roomName: roomNames[i],
            coordx: xs[i],
            coordy: ys[i],
            descrip: roomDescriptions[i]
        });
    }
    return rooms;
}

function getItemRoomInformation() {
    let itemRooms = [];
    let itemNames = [];
    let roomNames = [];
    $(".createItemRoomItem").each(function () {
        const itemName = $(this).val().toString();
        if (itemName !== "") itemNames.push(itemName);
    });
    $(".createItemRoomRoom").each(function () {
        const roomName = $(this).val().toString();
        if (roomName !== "") roomNames.push(roomName);
    });

    for (let i = 0; i < itemNames.length; i++) {
        itemRooms.push({
            itemName: itemNames[i],
            roomName: roomNames[i]
        });
    }

    return itemRooms;
}

function parseCommata(string) {
    let values = string.split(",");
    let finalValues = [];
    values.forEach(value => {
        if (value.startsWith(" ")) finalValues.push(value.substring(1))
        else finalValues.push(value)
    });
    return finalValues;
}

function getClassesAndRacesName(informationArray, jsonPropertyName) {
    const names = [];
    if (jsonPropertyName === "className") {
        informationArray.forEach(function (object) {
            names.push({"className": object.name});
        });
    } else if (jsonPropertyName === "raceName") {
        informationArray.forEach(function (object) {
            names.push({"raceName": object.name});
        });
    }
    return names;
}

function extractAllAbilities(classes, races) {
    const abilities = [];
    let abilitySet = new Set();
    classes.forEach(function (object) {
        object.abil.forEach(function (object) {
           abilitySet.add(object.toString())
        });
    });
    races.forEach(function (object) {
        object.abil.forEach(function (object) {
            abilitySet.add(object.toString());
        });
    });
    abilitySet.forEach(function (object) {
        abilities.push({"abilityName": object.toString()})
    });
    return abilities;
}

function getAbilityTuple(classesOrRaces, condition) {
    let tuples = [];
    if (condition === "raceName") {
        classesOrRaces.forEach(function (object) {
            object.abil.forEach(function (ability) {
                tuples.push({"raceName": object.name, "abilityName": ability.toString()});
            });
        });
    } else if (condition === "className") {
        classesOrRaces.forEach(function (object) {
            object.abil.forEach(function (ability) {
                tuples.push({"className": object.name, "abilityName": ability.toString()});
            });
        });
    }
    return tuples;
}

/**
 * Deletes all data in the form. For clean-up
 */
function cleanUpGameConfigForm () {
    $("#gameName").val("");
    $(".classCreate").each(function (index, object) {
        $(this).val("");
        if (index === 0) {
            $(this).siblings().each(function () {
                $(this).val("");
            });
        }
        if (index !== 0 && ($(this).parent().parent().parent().attr('style') !== 'display:none')) {
            $(this).parent().parent().parent().remove();
        }
    });
    $(".createRace").each(function (index, object) {
        $(this).val("");
        if (index === 0) {
            $(this).siblings().each(function () {
                $(this).val("");
            });
        }
        if (index !== 0 && ($(this).parent().parent().parent().attr('style') !== 'display:none')) {
            $(this).parent().parent().parent().remove();
        }
    });
    $(".createItem").each(function (index, object) {
        $(this).val("");
        if (index === 0) {
            $(this).siblings().each(function () {
                $(this).val("");
            });
        }
        if (index !== 0 && ($(this).parent().parent().parent().attr('style') !== 'display:none')) {
            $(this).parent().parent().parent().remove();
        }
    });
    $(".createRoom").each(function (index, object) {
        $(this).val("");
        if (index === 0) {
            $(this).siblings().each(function () {
                $(this).val("");
            });
        }
        if (index !== 0 && ($(this).parent().parent().parent().attr('style') !== 'display:none')) {
            $(this).parent().parent().parent().remove();
        }
    });
    $(".createItemRoomItem").each(function (index, object) {
        $(this).val("");
        if (index === 0) {
            $(this).siblings().each(function () {
                $(this).val("");
            });
        }
        if (index !== 0 && ($(this).parent().parent().parent().attr('style') !== 'display:none')) {
            $(this).parent().parent().parent().remove();
        }
    });
}

function gameConfigFormIsValid() {
    const commataRegEx = /^([^,]+)(((,[^,]+)*)?)$/
    const numbers = /^(0|((-)?[1-9][0-9]*))$/

    let messageString = "";

    let itemNamesList = [];
    let roomNameList = [];

    if ($("#gameName").val().toString().length === 0) {
        messageString += ("\nBitte gib' einen Spielnamen ein.");
    }
    $(".classCreate").each(function (index) {
        const currentObject = $(this);
        if (currentObject.parent().parent().parent().attr('style') !== 'display:none') {
            if (currentObject.val().toString().length === 0) {
                messageString += ("\nBitte gib' für die " + (index + 1) + ". Klasse einen Namen ein.")
            }
        }
    });
    $(".classCreateAbility").each(function (index) {
        const currentObject = $(this);
        if (currentObject.parent().parent().parent().attr('style') !== 'display:none') {
            if (currentObject.val().toString().length === 0) {
                messageString += "\nBitte gib' für die " + (index + 1) + ". Klasse min. 1 Fertigkeit an.";
            } else if (!commataRegEx.test(currentObject.val().toString())) {
                messageString += "\nÜberprüfe deine Fertigkeiteneingabe für Klasse " + (index + 1) + ". (Fertigkeit1, Fertigkeit2 ...)";
            }
        }
    });
    $(".createRace").each(function (index) {
        const currentObject = $(this);
        if (currentObject.parent().parent().parent().attr('style') !== 'display:none') {
            if (currentObject.val().toString().length === 0) {
                messageString += ("\nBitte gib' für die " + (index + 1) + ". Klasse einen Namen ein.")
            }
        }
    });
    $(".createRaceAbility").each(function (index) {
        const currentObject = $(this);
        if (currentObject.parent().parent().parent().attr('style') !== 'display:none') {
            if (currentObject.val().toString().length === 0) {
                messageString += "\nBitte gib' für die " + (index + 1) + ". Rasse min. 1 Fertigkeit an.";
            } else if (!commataRegEx.test(currentObject.val().toString())) {
                messageString += "\nÜberprüfe deine Fertigkeiteneingabe für Rasse " + (index + 1) + ". (Fertigkeit1, Fertigkeit2 ...)";
            }
        }
    });

    $(".createItem").each(function (index) {
        const currentObject = $(this);
        if (currentObject.parent().parent().parent().attr('style') !== 'display:none') {
            if (currentObject.val().toString().length === 0) {
                messageString += ("\nBitte gib' für das " + (index + 1) + ". Item einen Typ ein.")
            }
        }
    });
    $(".createItemName").each(function (index) {
        const currentObject = $(this);
        if (currentObject.parent().parent().parent().attr('style') !== 'display:none') {
            if (currentObject.val().toString().length === 0) {
                messageString += ("\nBitte gib' für das " + (index + 1) + ". Item einen Namen ein.")
            } else {
                itemNamesList.push(currentObject.val().toString());
            }
        }
    });
    $(".createItemValue").each(function (index) {
        const currentObject = $(this);
        if (currentObject.parent().parent().parent().attr('style') !== 'display:none') {
            if (currentObject.val().toString().length === 0) {
                currentObject.val("0");
            } else if (!numbers.test(currentObject.val().toString())) {
                messageString += ("\nBitte gib' für das " + (index + 1) + ". Item eine gültige Ganzzahl im Itemwert ein.")
            }
        }
    });
    $(".createRoom").each(function (index) {
        const currentObject = $(this);
        if (currentObject.parent().parent().parent().attr('style') !== 'display:none') {
            if (currentObject.val().toString().length === 0) {
                messageString += ("\nBitte gib' für den " + (index + 1) + ". Raum einen Namen ein.")
            } else {
                roomNameList.push(currentObject.val().toString())
            }
        }
    });

    let xCoords = [];
    let yCoords = [];

    $(".createRoomX").each(function (index) {
        const currentObject = $(this);
        if (currentObject.parent().parent().parent().attr('style') !== 'display:none') {
            if (currentObject.val().toString().length === 0) {
                messageString += ("\nBitte gib' für den " + (index + 1) + ". Raum eine X-Koordinate an.")
            } else if (!numbers.test(currentObject.val().toString())) {
                messageString += ("\nBitte gib' für den " + (index + 1) + ". Raum eine gültige Ganzzahl für die X-Koordinate an.")
            } else {
                xCoords.push(Number(currentObject.val().toString()))
            }
        }
    });
    $(".createRoomY").each(function (index) {
        const currentObject = $(this);
        if (currentObject.parent().parent().parent().attr('style') !== 'display:none') {
            if (currentObject.val().toString().length === 0) {
                messageString += ("\nBitte gib' für den " + (index + 1) + ". Raum eine Y-Koordinate an.")
            } else if (!numbers.test(currentObject.val().toString())) {
                messageString += ("\nBitte gib' für den " + (index + 1) + ". Raum eine gültige Ganzzahl für die Y-Koordinate an.")
            } else {
                yCoords.push(Number(currentObject.val().toString()))
            }
        }
    });

    let zeroZeroExsists = false;
    let tuple = [];
    xCoords.forEach(function (value, index) {
        if (xCoords[index] === 0 && yCoords[index] === 0) zeroZeroExsists = true;

    });

    if (!zeroZeroExsists) {
        messageString += "Füge einen Raum mit den Koordinaten (0, 0) hinzu.";
    }

    let areThereSameCoordinates = false;


    $(".createRoomDescription").each(function (index) {
        const currentObject = $(this);
        if (currentObject.parent().parent().parent().attr('style') !== 'display:none') {
            if (currentObject.val().toString().length === 0) {
                messageString += ("\nBitte gib' für den " + (index + 1) + ". Raum eine Beschreibung ein.")
            }
        }
    });


    $(".createItemRoomItem").each(function (index) {
        const currentObject = $(this);
        if (currentObject.parent().parent().parent().attr('style') !== 'display:none') {
            if (currentObject.val().toString().length === 0) {
                messageString += ("\nBitte gib' für die " + (index + 1) + ". Itemplatzierung einen Itemnamen ein.");
            } else if (!isInStringList(itemNamesList, currentObject.val().toString())) {
                messageString += ("\nIn der " + (index + 1) + ". Itemplatzierung korrespondiert das Item mit keinem der oben angegebenen");
            }
        }
    });
    $(".createItemRoomRoom").each(function (index) {
        const currentObject = $(this);
        if (currentObject.parent().parent().parent().attr('style') !== 'display:none') {
            if (currentObject.val().toString().length === 0) {
                messageString += ("\nBitte gib' für die " + (index + 1) + ". Itemplatzierung einen Raumnamen ein.")
            }  else if (!isInStringList(roomNameList, currentObject.val().toString())) {
                messageString += ("\nIn der " + (index + 1) + ". Itemplatzierung korrespondiert der Raum mit keinem der oben angegebenen");
            }
        }
    });

    if (messageString !== "") {
        window.alert(messageString);
        return false;
    } else {
        return true;
    }
}

function isInStringList (list, string) {
    let isInList = false;
    for (let i = 0; i < list.length; i++) {
        if (list[i] === string) {
            isInList = true;
            break;
        }
    }
    return isInList;
}

$(document).on("click", ".removeConfigElement", function () {
    $(this).parent().parent().remove();
});

let counterClass = 1;
function addClassField (){
    counterClass++;
    var newClassField = document.createElement('div');
    newClassField.id = counterClass;
    newClassField.innerHTML = document.getElementById('newClassField').innerHTML;
    document.getElementById('classContainer').appendChild(newClassField);
}

let counterRace = 1;
function addRaceField (){
    counterRace++;
    var newRaceField = document.createElement('div');
    newRaceField.id = counterRace;
    newRaceField.innerHTML = document.getElementById('newRaceField').innerHTML;
    document.getElementById('raceContainer').appendChild(newRaceField);
}

let counterItem = 1;
function addItemField (){
    counterItem++;
    var newItemField = document.createElement('div');
    newItemField.id = counterItem;
    newItemField.innerHTML = document.getElementById('newItemField').innerHTML;
    document.getElementById('itemContainer').appendChild(newItemField);
}

let counterRoom = 1;
function addRoomField (){
    counterRoom++;
    var newRoomField = document.createElement('div');
    newRoomField.id = counterRoom;
    newRoomField.innerHTML = document.getElementById('newRoomField').innerHTML;
    document.getElementById('roomContainer').appendChild(newRoomField);
}

let counterItemRoom = 1;
function addItemRoomField (){
    counterItemRoom++;
    var newItemRoomField = document.createElement('div');
    newItemRoomField.id = counterRace;
    newItemRoomField.innerHTML = document.getElementById('newItemRoomField').innerHTML;
    document.getElementById('itemRoomContainer').appendChild(newItemRoomField);
}
