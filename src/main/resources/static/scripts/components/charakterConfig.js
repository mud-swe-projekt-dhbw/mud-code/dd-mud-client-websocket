let classes = [];
let races = [];
let items = [];

/**
 * Function gets called when character config gets opened.
 * Fills the combo boxes with races and classes from the
 * corresponding game.
 */
function initCharacterConfig () {
    $("#chooseClass").empty();
    $("#chooseRace").empty();
    $.ajax({
        url: 'https://dd01.dv-xlab.de:49154/characterConfiguration/gameId/' + gameId,
        type: 'GET',
        success: function (response) {
            response.classDTOList.forEach(function (classObject, index) {
                classes.push(classObject);
                appendToClassOptions(classObject, index);
            });
            response.raceDTOList.forEach(function (raceObject, index) {
                races.push(raceObject);
                appendToRaceOptions(raceObject, index);
            });
            response.itemDTOList.forEach(function (itemObject, index) {
                items.push(itemObject);
                appendToItemOptions(itemObject, index);
            });
        },
        error: function (response) {
            if (response.status === 404) {
                window.alert("Mmh. Das Spiel, dass du gewählt hast, scheint nicht mehr zu exsistieren.")
            } else {
                window.alert("Oops. Etwas ist schiefgelaufen. Komm später zurück.");
            }
        }
    });
}

/**
 * Function appends class to combo box in HTML
 * @param classObject
 * @param index
 */
function appendToClassOptions (classObject, index) {
    const classComboBox = $("#chooseClass");
    classComboBox.append("<option value=\"class" + index + "\">" + classObject.className + "</option>");
}

/**
 * Function appends race to combo box in HTML
 * @param raceObject
 * @param index
 */
function appendToRaceOptions (raceObject, index) {
    const raceComboBox = $("#chooseRace");
    raceComboBox.append("<option value=\"race" + index + "\">" + raceObject.raceName + "</option>");
}

/**
 * Function appends item to combo box in HTML
 * @param itemObject
 * @param index
 */
function appendToItemOptions (itemObject, index) {
    const raceComboBox = $("#chooseItem");
    raceComboBox.append("<option value=\"item" + index + "\">" + itemObject.itemName + "</option>");
}

function commitNewCharacter () {
    $.ajax({
        url: 'https://dd01.dv-xlab.de:49154/characterConfiguration/gameId/' + gameId + '/new',
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(createCharacterJSON()),
        success: function () {
            cleanUpCharacterConfigForm();
            fromCharacterConfigToGame();
            connectToGameAndChat();
        },
        error: function (response) {
            switch (response.status) {
                case 400:
                    window.alert("Oops. Dein Charakter scheint fehlerhaft zu sein. Prüfe deine Eingaben noch einmal.");
                    break;
                case 404:
                    window.alert("Mmh. Das Spiel, dass du gewählt hast, scheint nicht mehr zu exsistieren.");
                    break;
                case 500:
                    window.alert("Ein unerwarteter Fehler ist aufgetreten. Versuche es später noch einmal.");
                    break;
            }

        }
    });
}

/**
 * Returns a JSON representing a character
 */
function createCharacterJSON () {
    return {
        "chName": $("#characterName").val().toString(),
        "classId": getCurrentClassId().toString(),
        "raceId": getCurrentRaceId().toString(),
        "itemId": getCurrentItemId().toString(),
        "userId": playerId,
        "description": $("#characterDescription").val().toString(),
        "personality": $("#characterPersonality").val().toString()
    }
}

/**
 * Function return the currentClassId
 * @return currentClassId, null else
 */
function getCurrentClassId() {
    const currentClassName = $("#chooseClass option:selected").text();
    let cId = null;
    classes.forEach(function (clazz, index) {
        if (currentClassName === clazz.className) {
            cId = clazz.classID;
        }
    });
    return cId;
}

/**
 * @return true if character already exists, false else
 */
async function checkIfPlayerAlreadyHasCharacter() {
    let b = false;
    await $.ajax({
        url: 'https://dd01.dv-xlab.de:49154/characterConfiguration/gameId/' + gameId + '/playerId/' + playerId + '/characterExistance',
        type: 'GET',
        success: function (response) {
            b = response.charakterAlreadyExists;
        },
        error: function () {
            window.alert("Leider konnten wir nicht ermitteln, ob du dem Spiel schon beigetreten bist.\n" +
                "Komm später noch einmal zurück.");
        }
    });
    return b;
}

/**
 * Function returns the current race id
 * @return current race id, null else
 */
function getCurrentRaceId() {
    const currentRaceName = $("#chooseRace option:selected").text();
    let rId = null;
    races.forEach(function (race, index) {
        if (currentRaceName === race.raceName) {
            rId = race.raceID;
        }
    });
    return rId;
}

function getCurrentItemId () {
    const currentItemName = $("#chooseItem option:selected").text();
    let iId = null;
    items.forEach(function (item, index) {
        if (currentItemName === item.itemName) {
            iId = item.itemId;
        }
    });
    return iId;
}

/**
 * Function cleans character config
 */
function returnBackFromCharacterConfig () {
    classes = [];
    races = [];
    items = [];
}

/**
 * Deletes all data from from. For clean up.
 */
function cleanUpCharacterConfigForm () {
    $("#chooseClass").empty();
    $("#chooseRace").empty();
    $("#chooseItem").empty();
    $("#characterName").val("");
    $("#characterDescription").val("");
    $("#characterPersonality").val("");
}