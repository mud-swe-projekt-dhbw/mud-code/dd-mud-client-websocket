const idRegEx = /^([1-9][0-9]*)$/;
const numberRegEx = /^(0)|((-)?[1-9][0-9]*)$/;

let classesDM = null;
let racesDM = null;


function sendNewAction () {
    const actionName = $("#newActionName").val().toString();
    const hp = $("#newActionResultHealth").val().toString();
    const dmg = $("#newActionResultDamage").val().toString();
    const newActionResult = $("#newActionResult").val().toString();
    const isGlobal = Boolean($("#actionIsGlobal").is(':checked'));
    const roomId = $("#newActionRoomID").val().toString();
    if ((actionName.length > 0 && newActionResult.length > 0 && numberRegEx.test(hp) && numberRegEx.test(dmg)
        && numberRegEx.test(dmg) && idRegEx.test(roomId) && !isGlobal) ||
        (actionName.length > 0 && newActionResult.length > 0 && numberRegEx.test(hp) && numberRegEx.test(dmg)
            && numberRegEx.test(dmg) && isGlobal)) {
        const actionTo = {
            actionName: actionName,
            actionResult: hp + ";" + dmg + ";" + newActionResult,
            roomId: Number(roomId),
            isGlobal: isGlobal
        };
        $.ajax({
            url: 'https://dd01.dv-xlab.de:49155/dungeonmaster/player/' + playerId + '/game/' + gameId + '/action',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(actionTo),
            success: function (gameObjects) {
                cleanUpNewAction();
                window.alert("Neue Aktion erfolgreich hinzugefügt!")
            },
            error: function () {
                window.alert("Beim Hinzufügen einer neuen Aktion ist ein Fehler aufgetreten.\n" +
                    "Achte darauf, dass die Raum-ID exsistiert, wenn deine Aktion raumbezogen ist.");
            }
        });
    } else {
        window.alert("Fehlerhafte Eingabe(n): Achte darauf dass HP und Damage Ganzzahlen sind und deine Raum ID gültig ist.\n" +
            "Außerdem müssen Name und textueller Ausgang der Aktion min. 1 Zeichen lang sein.")
    }
}

function sendNewRoomDescription() {
    const roomId = $("#changeRoomDescId").val().toString();
    const newDescription = $("#changeRoomDesc").val().toString();
    const descObject = {description: newDescription};
    if (idRegEx.test(roomId) && newDescription.length > 0) {
        $.ajax({
            url: 'https://dd01.dv-xlab.de:49155/dungeonmaster/player/' + playerId + '/game/' + gameId + '/room/' + roomId + '/new-description',
            type: 'PUT',
            contentType: 'application/json',
            data: JSON.stringify(descObject),
            success: function (gameObjects) {
                cleanUpUpdateDescription();
                window.alert("Aktualisierung erfolgreich!")
            },
            error: function () {
                window.alert("Beim Aktualisieren der Raumbeschreibung ist etwas schiefgelaufen!");
            }
        });
    } else {
        window.alert("Eingaben fehlerhaft. Gib' eine gültige ID ein und beachte, dass die Beschreibung min. ein Zeichen haben muss.")
    }
}

function sendNewRoom () {
    const newRoomName = $("#newRoomName").val().toString();
    const newRoomDesc = $("#newRoomDescription").val().toString();
    const xk = $("#newRoomX").val().toString();
    const yk = $("#newRoomY").val().toString();
    const roomItemType = $("#newItemInRoomDMType").val().toString();
    const roomItemName = $("#newItemInRoomDMName").val().toString();
    const roomItemValue = $("#newItemInRoomDMValue").val().toString();
    const roomActionName = $("#newActionInRoomDM").val().toString();
    const roomHP = $("#newActionResultHealthRoom").val().toString();
    const roomDMG = $("#newActionResultDamageRoom").val().toString();
    const roomActionText = $("#newActionResultRoom").val().toString();

    if (
        newRoomName.length > 0 &&
        newRoomDesc.length > 0 &&
        numberRegEx.test(xk) &&
        numberRegEx.test(yk) &&
        roomItemType.length > 0 &&
        roomItemName.length > 0 &&
        numberRegEx.test(roomItemValue) &&
        roomActionName.length > 0 &&
        numberRegEx.test(roomHP) &&
        numberRegEx.test(roomDMG) &&
        roomActionText.length > 0
    ) {
        const item = [{
            itemType: roomItemType,
            itemName: roomItemName,
            itemValue: roomItemValue
        }];
        const action = [{
            actionName: roomActionName,
            actionResult: roomHP + ";" + roomDMG + ";" + roomActionText
        }];
        const roomObject = {
            roomName: newRoomName,
            x: xk,
            y: yk,
            description: newRoomDesc,
            actions : action,
            items: item
        };
        $.ajax({
            url: 'https://dd01.dv-xlab.de:49155/dungeonmaster/player/' + playerId + '/game/' + gameId + '/room',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(roomObject),
            success: function (gameObjects) {
                cleanUpNewRoom();
                window.alert("Raum-Erstellung erfolgreich!")
            },
            error: function () {
                window.alert("Bei der Übermittlung eines neuen Raums gab' es einen Fehler");
            }
        });
    } else {
        window.alert("Es gibt ein Fehler im Formular. Beachte, dass du korrekte Zahlen eingibst und Text-Argumente min. ein Zeichen lang sein müssen.");
    }
}

function banPlayer() {
    const inputElement = $("#banPlayerId");
    const playerBanId = inputElement.val().toString();
    if (idRegEx.test(playerBanId)) {
        $.ajax({
            url: 'https://dd01.dv-xlab.de:49155/dungeonmaster/player/' + playerId + '/game/' + gameId + '/ban/' + playerBanId,
            type: 'POST',
            success: function (gameObjects) {
                cleanUpBanPlayer();
                window.alert("Ausschluss erfolgreich!")
            },
            error: function () {
                window.alert("Beim Spieler bannen ist ein Fehler passiert. Prüfe deine Eingabe.\nExsistiert diese Spieler-ID?");
            }
        });
    } else {
        window.alert("Bitte gib' eine gültige Zahl ein!");
    }
}

async function initDungeonMaster() {
    await getGameInformation();
    await connectToWebSocket();
}

async function getGameInformation() {
    await $.ajax({
        url: 'https://dd01.dv-xlab.de:49155/dungeonmaster/player/' + playerId + '/game/' + gameId + '/information',
        type: 'GET',
        success: function (response) {
            let answerText = $("#gameInfoText").text(response.information.toString());
            answerText.html(answerText.html().replace(/\n/g, '<br/>'));
            classesDM = response.classTOS;
            racesDM = response.raceTOS;
        },
        error: function () {
            window.alert("Beim holen der Spielinformationen ist etwas schiefgelaufen.\nVersuche es später noch einmal!");
        }
    });
}

function connectToWebSocket () {
    let socket = new SockJS('/mud-ws');
    stompClient = Stomp.over(socket);
    subscribeToTopics();
}

function subscribeToTopics() {
    stompClient.connect({}, function (frame) {
        stompClient.subscribe('/topic/chat/player/' + playerId, function (message) {
            receiveChatMessage(JSON.parse(message.body));
        });
        stompClient.subscribe('topic/dungeonmaster/player/' + playerId  + "/game/" + gameId, function (message) {
            receiveCompactGame(JSON.parse(message.body));
        });
        sendToGameService("START!;");
    });
}

function sendChatMessage () {
    const chatMessageInput = $("#chatInputDM");
    const message = chatMessageInput.val().toString();
    const route = "/mud-app/chat/game/" + gameId + "/player/" + playerId + "/input";
    stompClient.send(route, {}, JSON.stringify({name: username,
        message: message}));
    chatMessageInput.val("");
}

function receiveChatMessage (message) {
    const chatArea = $("#chatTextAreaDM");
    const messageString = message.name.toString() + ": " + message.message.toString();

    chatArea.val(chatArea.val() + "\n" + messageString);
}

function getRaceClassString(classId, raceId) {
    let clazz = "";
    let race = "";
    classesDM.forEach(object => {
        if (object.classId === classId) clazz = object.className;
    });
    racesDM.forEach(object => {
        if (object.raceId === raceId) race = object.raceName;
    });
    return clazz + "|" + race;
}

function receiveCompactGame (message) {
    const rooms = $("#tBodyRoomOverview");
    const globalActions = $("#tBodyGlobalActions");
    const characters = $("#tBodyCharacterOverview");
    rooms.empty();
    globalActions.empty();
    characters.empty();

    const gActionsJson = message.globalActions;
    gActionsJson.forEach(function (object, index) {
        globalActions.append("<tr><th scope='row'>" + (index + 1) + "</th><td>" + object.actionName + "</td></tr>");
    });

    const charactersJSON = [];

    message.rooms.forEach(function (object, index) {
        let characterString = "";
        let actionsString = "";
        object.charactersInRoom.forEach(object => {
            charactersJSON.push(object);
            characterString += object.name + " (" + object.userName + ")\n";
        });
        object.actions.forEach(object => {
            actionsString += object.actionName + " ";
        })
        rooms.append(createRoomTableRow(
            index,
            object.roomName,
            object.coordx,
            object.coordy,
            object.description,
            characterString,
            actionsString,
            object.roomId
        ));
    });

    charactersJSON.forEach(function (object, index) {
        let itemsString = "";
        object.characterItems.forEach(object => {itemsString += object.itemName + "(" + object.itemType + ")" +
            " Wert: " + object.itemValue});
        characters.append(createCharacterTableRow(
            index,
            object.name,
            object.userName,
            getRaceClassString(object.classId, object.raceId),
            object.hp,
            object.damage,
            itemsString,
            object.playerId
        ));
    });
}

function createRoomTableRow(index, roomname, x, y, desc, characters, actions, id) {
    return "<tr><th scope='row'>" + id + "</th><td>" + roomname + "</td><td>" + x  + "|" +  y + "</td><td>" + desc + "</td><td>" + characters
        + "</td><td>" + actions + "</td><td>" + id + "</td></tr>";
}

function createCharacterTableRow(index, name, user, raceClass, hp, dmg, items, id) {
    return "<tr><th scope='row'>" + id + "</th><td>" + name + "</td><td>" + user + "</td><td>" + raceClass + "</td>" +
        "<td>" + hp + "</td><td>" + dmg + "</td><td>" + items + "</td><td>" + id + "</td></tr>";
}

function sendToGameService (newMessage) {
    const route = "/mud-app/game/" + gameId + "/player/" + playerId + "/input";
    stompClient.send(route, {}, JSON.stringify({message: newMessage}));
}

function cleanUpBanPlayer () {
    $("#banPlayerId").val("");
}

function cleanUpNewAction () {
    $("#newActionName").val("");
    $("#newActionResultHealth").val("");
    $("#newActionResultDamage").val("");
    $("#newActionResult").val("");
    $("#newActionRoomID").val("");
}

function cleanUpUpdateDescription() {
    $("#changeRoomDescId").val("");
    $("#changeRoomDesc").val("");
}

function deleteGame() {
    if (confirm("Willst du das Spiel wirklich beenden?")) {
        sendToGameService("RESET!;");
        returnToChooseGame("dm");
    }
}

function cleanUpNewRoom () {
    $("#newRoomName").val("");
    $("#newRoomDescription").val("");
    $("#newRoomX").val("");
    $("#newRoomY").val("");
    $("#newItemInRoomDMType").val("");
    $("#newItemInRoomDMName").val("");
    $("#newItemInRoomDMValue").val("");
    $("#newActionInRoomDM").val("");
    $("#newActionResultHealthRoom").val("");
    $("#newActionResultDamageRoom").val("");
    $("#newActionResultRoom").val("");
}

function cleanUpDM () {
    cleanUpNewRoom();
    cleanUpNewAction();
    cleanUpUpdateDescription();
    cleanUpBanPlayer();
    $("#chatTextAreaDM").val("");
    $("#chatTextAreaDM").text("");
    $("#tBodyCharacterOverview").empty();
    $("#tBodyGlobalActions").empty();
    $("#tBodyRoomOverview").empty();
}


$("#actionIsGlobal").click(function () {
    let roomIdInput = $("#newActionRoomID");
    if ($(this).is(':checked')) {
        roomIdInput.prop('disabled', true);
    } else {
        roomIdInput.prop('disabled', false);
    }
});