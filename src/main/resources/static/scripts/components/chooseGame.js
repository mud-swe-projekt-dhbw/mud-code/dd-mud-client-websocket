// Game objects with local client id
let games = [];

/**
 * Function fetches games from service
 * @return games object
 */
async function getGames(playerId) {
    await $.ajax({
       url: 'https://dd01.dv-xlab.de:49153/games/mud-game-objects',
       type: 'GET',
        success: function (gameObjects) {
            games = [];
            gameObjects.forEach(function (object, index) {
                games.push(createLocalGameObject(object, ++index));
            });
            fillGameLists(playerId);
        },
        error: function () {
            window.alert("Beim laden der verfügbaren Spiele ist etwas schiefgelaufen.\nBitte versuche es später noch mal");
        }
    });
}

function createLocalGameObject(game, number) {
    return {
        number: number,
        gameName: game.gameName,
        userId: game.userId,
        gameId: game.gameId
    };
}

/**
 * Function fills lists in the overview
 * @param playerId
 */
function fillGameLists(playerId) {
    const tableBodyNotDM = $("#tBodyJoinGameNotDM");
    const tableBodyDM = $("#tBodyJoinGameDM");
    tableBodyNotDM.empty();
    tableBodyDM.empty();
    games.forEach(function (object, index) {
        if (object.userId === playerId) {
            tableBodyDM.append("<tr class='dmTable-row' data-number='" + object.number+ "'><th scope='row'>" + object.number + "</th><td>" + object.gameName + "</td></tr>");
        } else {
            tableBodyNotDM.append("<tr class='notDMTable-row' data-number='" + object.number+ "'><th scope='row'>" + object.number + "</th><td>" + object.gameName + "</td></tr>");
        }
    });
}

/**
 * Function for joining normal game
 */
async function chooseGame(localGameNumber) {
    let game = null;
    games.forEach(function (object, index) {
        if (index === localGameNumber - 1) game = object;
    });

    // DM-Games
    if (game !== null) {
        gameId = game.gameId;
        if (playerId === game.userId) {
            fromChooseGameToDM();
            await initDungeonMaster();
        } else { // Non DM-Games
            if (await checkIfPlayerAlreadyHasCharacter()) {
                fromChooseGameToGame();
                await connectToGameAndChat();
            } else {
                initCharacterConfig();
                fromChooseGameToCharacterConfig();
            }
        }
    }
}


/**
 * Function triggered when the new game button is pushed
 */
function configNewGame() {
    fromChooseGameToGameConfig();
}

function deleteGames () {
    games = [];
}