function connectToGameAndChat () {
    var socket = new SockJS('/mud-ws');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        stompClient.subscribe('/topic/chat/player/' + playerId, function (message) {
            handleIncomingChatMessage(JSON.parse(message.body));
        });
        stompClient.subscribe('/topic/game/player/' + playerId + "/game/" + gameId, function (message) {
            handleIncomingGameMessage(JSON.parse(message.body));
        });
        const route = "/mud-app/game/" + gameId + "/player/" + playerId + "/input";
        stompClient.send(route, {}, JSON.stringify({message: "START!;"}));
    });
}


function handleIncomingGameMessage(message) {
    const gameArea = $("#gameTextArea");
    gameArea.val(gameArea.val() + "\n\n" + message.message.toString())

}

function handleIncomingChatMessage(message) {
    const chatArea = $("#chatTextArea");
    const chatString = message.name + ": " + message.message;
    chatArea.val(chatArea.val() + "\n" + chatString);
}

function sendToGame() {
    const gameInput = $("#gameInput");
    const newMessage = gameInput.val().toString();
    const route = "/mud-app/game/" + gameId + "/player/" + playerId + "/input";
    stompClient.send(route, {}, JSON.stringify({message: newMessage}));
    const gameArea = $("#gameTextArea");
    gameArea.val(gameArea.val() + "\n" + newMessage);
    gameInput.val("");
}

function sendToChat() {
    const chatInput = $("#chatInput");
    const newMessage = chatInput.val().toString();
    const route = "/mud-app/chat/game/" + gameId + "/player/" + playerId + "/input";
    stompClient.send(route, {}, JSON.stringify({name: username,
        message: newMessage}));
    chatInput.val("");
}

function disconnectFromGame() {
    const route = "/mud-app/game/" + gameId + "/player/" + playerId + "/input";
    stompClient.send(route, {}, JSON.stringify({message: "EXIT!;"}));
    if (stompClient !== null) {
        stompClient.disconnect();
    }
    stompClient = null;
    $("#gameInput").val("");
    $("#chatInput").val("");
    $("#gameTextArea").val("");
    $("#chatTextArea").val("");
}

function deleteCharakterFromGame() {
    if (confirm("Bist du dir sicher, deinen Charakter in diesem Spiel zu löschen?")) {
        $.ajax({
            url: 'https://dd01.dv-xlab.de:49154/characterConfiguration/delete/gameId/' + gameId + '/playerId/' + playerId,
            type: 'DELETE',
            success: function () {
                returnToChooseGame('game')
            },
            error: function () {
                window.alert("Mmh. Etwas ist schiefgelaufen. Versuche es später noch einmal.");
            }
        });
    }
}