let stompClient = null;
let username = null;
let playerId = null;
let gameId = null;

$("#loginForm").submit(function(e){
    e.preventDefault();
    login();
});

$("#signUpForm").submit(function(e){
    e.preventDefault();
    signUp();
});

$("#passwordForgottonForm").submit(function(e){
    e.preventDefault();
    forgotPassword();
});

$("#gameConfigFrom").submit(function (e) {
    e.preventDefault();
});

$("#characterConfigFrom").submit(async function (e) {
    e.preventDefault();
    await commitNewCharacter();
});

$("#chatInputForm").submit(function (e) {
    e.preventDefault();
});

$("#gameInputForm").submit(function (e) {
    e.preventDefault();
});

$("#chatInputFormDM").submit(function (e) {
    e.preventDefault();
});

$("#changeRoomDescForm").submit(function (e) {
    e.preventDefault();
    sendNewRoomDescription();
});

$("#banPlayerDM").submit(function (e) {
    e.preventDefault();
    banPlayer();
});

$("#newActionForm").submit(function (e) {
    e.preventDefault();
    sendNewAction();
});

$("#newRoomForm").submit(function (e) {
    e.preventDefault();
    sendNewRoom();
});

$("#chatInput").on("keyup", function (e) {
    e.preventDefault();
    if (e.keyCode === 13) {
        sendToChat();
    }
});

$("#gameInput").on("keyup", function (e) {
    e.preventDefault();
    if (e.keyCode === 13) {
        sendToGame();
    }
});

$("#chatInputDM").on("keyup", function (e) {
   e.preventDefault();
   if (e.keyCode === 13) {
       sendChatMessage();
   }
});

$('#tBodyJoinGameNotDM').on('click', 'tr', function() {
    const number = Number($(this).find("th").text());
    chooseGame(number);
});

$('#tBodyJoinGameDM').on('click', 'tr', function() {
    const number = Number($(this).find("th").text());
    chooseGame(number);
});

function returnToChooseGame(source) {
    switch (source) {
        case "characterConfig":
            returnBackFromCharacterConfig();
            fromCharacterConfigToChooseGame();
            cleanUpCharacterConfigForm();
            break;
        case "dm":
            gameId = null;
            cleanUpDM();
            fromDMToChooseGame();
            break;
        case "gameConfig":
            cleanUpGameConfigForm();
            fromGameConfigToChooseGame();
            break;
        case "game":
            disconnectFromGame();
            fromGameToChooseGame();
            gameId = null;
    }
    getGames(playerId);
}

function disconnect() {
    playerId = null;
    username = null;
    deleteGames();
    fromChooseGameToLogin();
}