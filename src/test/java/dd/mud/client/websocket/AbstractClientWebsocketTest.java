package dd.mud.client.websocket;

import dd.mud.client.websocket.boundary.model.kafka.ChatServiceAnswer;
import dd.mud.client.websocket.boundary.model.kafka.CompactGame;
import dd.mud.client.websocket.boundary.model.kafka.MudKafkaMessage;
import dd.mud.client.websocket.boundary.model.websocket.ChatMessage;
import dd.mud.client.websocket.boundary.model.websocket.SimpleTextMessage;

import java.util.Arrays;

public abstract class AbstractClientWebsocketTest {

    protected MudKafkaMessage getMudKafkaMessage () {
        return MudKafkaMessage.builder()
                .id(1L)
                .message("Test Message")
                .gameId(2L)
                .build();
    }

    protected ChatServiceAnswer getChatServiceAnswer () {
        return ChatServiceAnswer.builder()
                .recipients(Arrays.asList(1L, 2L, 3L, 4L, 5L, 6L))
                .senderId(7L)
                .message("Test message")
                .build();
    }

    protected CompactGame getCompactGame () {
        return CompactGame.builder()
                .gameId(1L)
                .dungeonMasterId(2L)
                .build();
    }

    protected ChatMessage getChatMessage () {
        return ChatMessage.builder()
                .name("Name")
                .message("Message")
                .build();
    }

    protected SimpleTextMessage getSimpleTextMessage () {
        return SimpleTextMessage.builder()
                .message("Test message")
                .build();
    }
}
