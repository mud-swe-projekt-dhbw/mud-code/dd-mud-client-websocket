package dd.mud.client.websocket.boundary.websocket.receiver;

import dd.mud.client.websocket.AbstractClientWebsocketTest;
import dd.mud.client.websocket.boundary.model.websocket.ChatMessage;
import dd.mud.client.websocket.controller.service.forward.ChatMessageForwardingService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

@RunWith(MockitoJUnitRunner.class)
class ChatSocketApiTest extends AbstractClientWebsocketTest {

    @Mock
    private ChatMessageForwardingService chatMessageForwardingService;

    @InjectMocks
    private ChatSocketApi chatSocketApi;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void receiveChatMessageFromPlayer() {
        doNothing().when(chatMessageForwardingService).forwardChatMessage(eq(1L), eq(2L), any(ChatMessage.class));

        chatSocketApi.receiveChatMessageFromPlayer(1L, 2L, getChatMessage());

        verify(chatMessageForwardingService, times(1)).forwardChatMessage( eq(2L), eq(1L), any(ChatMessage.class));
        verifyNoMoreInteractions(chatMessageForwardingService);
    }
}