package dd.mud.client.websocket.boundary.websocket.receiver;

import dd.mud.client.websocket.AbstractClientWebsocketTest;
import dd.mud.client.websocket.boundary.model.websocket.ChatMessage;
import dd.mud.client.websocket.boundary.model.websocket.SimpleTextMessage;
import dd.mud.client.websocket.controller.service.forward.ChatMessageForwardingService;
import dd.mud.client.websocket.controller.service.forward.GameMessageForwardingService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

@RunWith(MockitoJUnitRunner.class)
class GameSocketApiTest extends AbstractClientWebsocketTest {

    @Mock
    private GameMessageForwardingService gameMessageForwardingService;

    @InjectMocks
    private GameSocketApi gameSocketApi;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void receiveChatMessageFromPlayer() {
        doNothing().when(gameMessageForwardingService).forwardGameMessage(eq(1L), eq(2L), any(SimpleTextMessage.class));

        gameSocketApi.receiveGameMessage(1L, 2L, getSimpleTextMessage());

        verify(gameMessageForwardingService, times(1)).forwardGameMessage( eq(2L),eq(1L), any(SimpleTextMessage.class));
        verifyNoMoreInteractions(gameMessageForwardingService);
    }
}