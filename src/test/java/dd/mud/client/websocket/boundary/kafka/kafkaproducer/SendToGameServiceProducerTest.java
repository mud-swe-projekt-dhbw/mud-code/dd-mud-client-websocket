package dd.mud.client.websocket.boundary.kafka.kafkaproducer;

import dd.mud.client.websocket.AbstractClientWebsocketTest;
import dd.mud.client.websocket.boundary.model.kafka.MudKafkaMessage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.kafka.core.KafkaTemplate;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

class SendToGameServiceProducerTest extends AbstractClientWebsocketTest {

    @Mock
    private KafkaTemplate<String, MudKafkaMessage> kafkaTemplate;

    @InjectMocks
    private SendToGameServiceProducer sendToGameServiceProducer;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void sendToChatService() {
        doReturn(null).when(kafkaTemplate).send(eq("gameGameMessage.t"), any(MudKafkaMessage.class));

        sendToGameServiceProducer.sendToGameService(getMudKafkaMessage());

        verify(kafkaTemplate, times(1)).send(eq("gameGameMessage.t"), any(MudKafkaMessage.class));
        verifyNoMoreInteractions(kafkaTemplate);
    }
}