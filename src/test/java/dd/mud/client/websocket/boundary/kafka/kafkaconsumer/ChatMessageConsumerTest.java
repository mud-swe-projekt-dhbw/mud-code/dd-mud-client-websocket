package dd.mud.client.websocket.boundary.kafka.kafkaconsumer;

import dd.mud.client.websocket.AbstractClientWebsocketTest;
import dd.mud.client.websocket.boundary.model.kafka.MudKafkaMessage;
import dd.mud.client.websocket.controller.service.answer.ChatServiceAnswerService;
import dd.mud.client.websocket.controller.service.answer.GameServiceAnswerService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

@RunWith(MockitoJUnitRunner.class)
class ChatMessageConsumerTest extends AbstractClientWebsocketTest {
    @Mock
    private ChatServiceAnswerService chatServiceAnswerService;

    @InjectMocks
    private ChatMessageConsumer chatMessageConsumer;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void receiveGameServiceAnswer() {
        doNothing().when(chatServiceAnswerService).processChatServiceAnswer(any());

        chatMessageConsumer.receiveChatServiceAnswer(getChatServiceAnswer());

        verify(chatServiceAnswerService, times(1)).processChatServiceAnswer(any());
        verifyNoMoreInteractions(chatServiceAnswerService);
    }
}