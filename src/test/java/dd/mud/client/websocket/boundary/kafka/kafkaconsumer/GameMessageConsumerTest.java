package dd.mud.client.websocket.boundary.kafka.kafkaconsumer;

import dd.mud.client.websocket.AbstractClientWebsocketTest;
import dd.mud.client.websocket.boundary.model.kafka.MudKafkaMessage;
import dd.mud.client.websocket.controller.service.answer.GameServiceAnswerService;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.KafkaListenerEndpointRegistry;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.listener.MessageListenerContainer;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.kafka.test.rule.EmbeddedKafkaRule;
import org.springframework.kafka.test.utils.ContainerTestUtils;
import org.springframework.kafka.test.utils.KafkaTestUtils;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import javax.sound.midi.MidiMessage;
import javax.sound.midi.Receiver;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

@RunWith(MockitoJUnitRunner.class)
class GameMessageConsumerTest extends AbstractClientWebsocketTest {

    @Mock
    private GameServiceAnswerService gameServiceAnswerService;

    @InjectMocks
    private GameMessageConsumer gameMessageConsumer;


    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void receiveGameServiceAnswer() {
        doNothing().when(gameServiceAnswerService).processGameServiceAnswer(any());

        MudKafkaMessage mudKafkaMessage = getMudKafkaMessage();
        gameMessageConsumer.receiveGameServiceAnswer(mudKafkaMessage);

        verify(gameServiceAnswerService, times(1)).processGameServiceAnswer(any());
        verifyNoMoreInteractions(gameServiceAnswerService);
    }
}