package dd.mud.client.websocket.boundary.kafka.kafkaproducer;

import dd.mud.client.websocket.AbstractClientWebsocketTest;
import dd.mud.client.websocket.boundary.model.kafka.MudKafkaMessage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.kafka.core.KafkaTemplate;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
class SendToChatServiceProducerTest extends AbstractClientWebsocketTest {

    @Mock
    private KafkaTemplate<String, MudKafkaMessage> kafkaTemplate;

    @InjectMocks
    private SendToChatServiceProducer sendToChatServiceProducer;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void sendToChatService() {
        doReturn(null).when(kafkaTemplate).send(eq("chatChatMessage.t"), any(MudKafkaMessage.class));

        sendToChatServiceProducer.sendToChatService(getMudKafkaMessage());

        verify(kafkaTemplate, times(1)).send(eq("chatChatMessage.t"), any(MudKafkaMessage.class));
        verifyNoMoreInteractions(kafkaTemplate);
    }
}