package dd.mud.client.websocket.boundary.kafka.kafkaconsumer;

import dd.mud.client.websocket.AbstractClientWebsocketTest;
import dd.mud.client.websocket.boundary.model.kafka.MudKafkaMessage;
import dd.mud.client.websocket.controller.service.answer.DMGameUpdateService;
import dd.mud.client.websocket.controller.service.answer.GameServiceAnswerService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

class DungeonMasterConsumerTest extends AbstractClientWebsocketTest {

    @Mock
    private DMGameUpdateService dmGameUpdateService;

    @InjectMocks
    private DungeonMasterConsumer dungeonMasterConsumer;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void receiveGameServiceAnswer() {
        doNothing().when(dmGameUpdateService).sendCurrentGameToClient(any());

        dungeonMasterConsumer.getGameUpdateFromDMService(getCompactGame());

        verify(dmGameUpdateService, times(1)).sendCurrentGameToClient(any());
        verifyNoMoreInteractions(dmGameUpdateService);
    }
}