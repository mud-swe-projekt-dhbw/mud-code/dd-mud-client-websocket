package dd.mud.client.websocket.boundary.websocket.sender;

import dd.mud.client.websocket.AbstractClientWebsocketTest;
import dd.mud.client.websocket.boundary.model.websocket.ChatMessage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.messaging.simp.SimpMessagingTemplate;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

@RunWith(MockitoJUnitRunner.class)
class ChatPlayerTopicSenderTest extends AbstractClientWebsocketTest {

    @Mock
    private SimpMessagingTemplate template;

    @InjectMocks
    private ChatPlayerTopicSender chatPlayerTopicSender;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void sendToPlayerChatWebsocketTopic() {
        doNothing().when(template).convertAndSend(eq("/topic/chat/player/1"), any(ChatMessage.class));

        chatPlayerTopicSender.sendToPlayerChatWebsocketTopic(1L, getChatMessage());

        verify(template, times(1)).convertAndSend(eq("/topic/chat/player/1"), any(ChatMessage.class));
        verifyNoMoreInteractions(template);
    }
}