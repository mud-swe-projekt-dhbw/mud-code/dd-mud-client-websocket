package dd.mud.client.websocket.boundary.websocket.sender;

import dd.mud.client.websocket.AbstractClientWebsocketTest;
import dd.mud.client.websocket.boundary.model.kafka.CompactGame;
import dd.mud.client.websocket.boundary.model.websocket.ChatMessage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.messaging.simp.SimpMessagingTemplate;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

@RunWith(MockitoJUnitRunner.class)
class DMPlayerTopicSenderTest extends AbstractClientWebsocketTest {

    @Mock
    private SimpMessagingTemplate template;

    @InjectMocks
    private DMPlayerTopicSender dmPlayerTopicSender;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

//    @Test
//    void sendToPlayerChatWebsocketTopic() {
//        doNothing().when(template).convertAndSend(eq("topic/dungeonmaser/player/1"), any(CompactGame.class));
//
//        dmPlayerTopicSender.sendToPlayerDMWebsocket(1L, getCompactGame());
//
//        verify(template, times(1)).convertAndSend(eq("topic/dungeonmaser/player/1"), any(CompactGame.class));
//        verifyNoMoreInteractions(template);
//    }
}