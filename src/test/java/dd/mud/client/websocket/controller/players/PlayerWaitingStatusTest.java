package dd.mud.client.websocket.controller.players;

import dd.mud.client.websocket.AbstractClientWebsocketTest;
import org.junit.Before;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(MockitoJUnitRunner.class)
class PlayerWaitingStatusTest extends AbstractClientWebsocketTest {

    @InjectMocks
    private PlayerWaitingStatus playerWaitingStatus;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        this.playerWaitingStatus.getPlayerWaitingMap().put(1L, true);
        this.playerWaitingStatus.getPlayerWaitingMap().put(2L, true);
        this.playerWaitingStatus.getPlayerWaitingMap().put(3L, false);
        this.playerWaitingStatus.getPlayerWaitingMap().put(4L, false);
    }

    @Test
    public void checkAndLock() {
        Boolean actual = playerWaitingStatus.checkAndLock(1L);
        assertEquals(true, actual);
        actual = this.playerWaitingStatus.checkAndLock(1L);
        assertEquals(false, actual);
        actual = this.playerWaitingStatus.checkAndLock(5L); // New Player
        assertEquals(true, actual);
    }

    @Test
    public void changeStatusOfPlayer() {
        this.playerWaitingStatus.changeStatusOfPlayer(1L, false);
        Boolean actual = this.playerWaitingStatus.getPlayerWaitingMap().get(1L);
        assertEquals(false, actual);
        this.playerWaitingStatus.changeStatusOfPlayer(5L, true);
        actual = this.playerWaitingStatus.getPlayerWaitingMap().get(5L);
        assertNull(actual);
    }

    @Test
    public void removePlayer() {
        this.playerWaitingStatus.removePlayer(1L);
        Boolean actual = this.playerWaitingStatus.getPlayerWaitingMap().get(1L);
        assertNull(actual);
    }
}