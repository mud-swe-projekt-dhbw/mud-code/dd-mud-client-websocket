package dd.mud.client.websocket.controller.service.answer;

import dd.mud.client.websocket.AbstractClientWebsocketTest;
import dd.mud.client.websocket.boundary.model.kafka.CompactGame;
import dd.mud.client.websocket.boundary.websocket.sender.DMPlayerTopicSender;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

@RunWith(MockitoJUnitRunner.class)
class DMGameUpdateServiceTest extends AbstractClientWebsocketTest {

    @Mock
    private DMPlayerTopicSender dmPlayerTopicSender;

    @InjectMocks
    private DMGameUpdateService dmGameUpdateService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void sendCurrentGameToClient() {
        doNothing().when(dmPlayerTopicSender).sendToPlayerDMWebsocket(anyLong(), anyLong(), any());

        CompactGame compactGame = getCompactGame();
        this.dmGameUpdateService.sendCurrentGameToClient(compactGame);

        verify(dmPlayerTopicSender, times(1)).sendToPlayerDMWebsocket(2L, 1L, compactGame);
        verifyNoMoreInteractions(dmPlayerTopicSender);
    }
}