package dd.mud.client.websocket.controller.service.answer;

import dd.mud.client.websocket.AbstractClientWebsocketTest;
import dd.mud.client.websocket.boundary.websocket.sender.GamePlayerTopicSender;
import dd.mud.client.websocket.controller.players.PlayerWaitingStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;

@RunWith(MockitoJUnitRunner.class)
class GameServiceAnswerServiceTest extends AbstractClientWebsocketTest {

    @Mock
    private PlayerWaitingStatus playerWaitingStatus;

    @Mock
    private GamePlayerTopicSender gamePlayerTopicSender;

    @InjectMocks
    private GameServiceAnswerService gameServiceAnswerService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void processGameServiceAnswer() {
        doNothing().when(gamePlayerTopicSender).sendToPlayerGameWebsocket(anyLong(), anyLong(), any());
        doNothing().when(playerWaitingStatus).changeStatusOfPlayer(anyLong(), anyBoolean());

        gameServiceAnswerService.processGameServiceAnswer(getMudKafkaMessage());

        verify(gamePlayerTopicSender, times(1)).sendToPlayerGameWebsocket(anyLong(), anyLong(), any());
        verify(playerWaitingStatus, times(1)).changeStatusOfPlayer(anyLong(), anyBoolean());
        verifyNoMoreInteractions(gamePlayerTopicSender, playerWaitingStatus);
    }
}