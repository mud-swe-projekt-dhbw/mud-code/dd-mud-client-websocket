package dd.mud.client.websocket.controller.service.forward;

import dd.mud.client.websocket.AbstractClientWebsocketTest;
import dd.mud.client.websocket.boundary.kafka.kafkaproducer.SendToGameServiceProducer;
import dd.mud.client.websocket.controller.players.PlayerWaitingStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

@RunWith(MockitoJUnitRunner.class)
class GameMessageForwardingServiceTest extends AbstractClientWebsocketTest {

    @Mock
    private PlayerWaitingStatus playerWaitingStatus;

    @Mock
    private SendToGameServiceProducer sendToGameServiceProducer;

    @InjectMocks
    private GameMessageForwardingService gameMessageForwardingService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void forwardGameMessagePlayerAllowed () {
        doReturn(Boolean.TRUE).when(playerWaitingStatus).checkAndLock(anyLong());
        doNothing().when(sendToGameServiceProducer).sendToGameService(any());

        gameMessageForwardingService.forwardGameMessage(1L, 2L, getSimpleTextMessage());

        verify(playerWaitingStatus, times(1)).checkAndLock(anyLong());
        verify(sendToGameServiceProducer, times(1)).sendToGameService(any());
        verifyNoMoreInteractions(playerWaitingStatus, sendToGameServiceProducer);
    }

    @Test
    public void forwardGameMessagePlayerNotAllowed () {
        doReturn(Boolean.FALSE).when(playerWaitingStatus).checkAndLock(anyLong());
        doNothing().when(sendToGameServiceProducer).sendToGameService(any());

        gameMessageForwardingService.forwardGameMessage(1L, 2L, getSimpleTextMessage());

        verify(playerWaitingStatus, times(1)).checkAndLock(anyLong());
        verify(sendToGameServiceProducer, times(0)).sendToGameService(any());
        verifyNoMoreInteractions(playerWaitingStatus, sendToGameServiceProducer);
    }
}