package dd.mud.client.websocket.controller.service.forward;

import dd.mud.client.websocket.AbstractClientWebsocketTest;
import dd.mud.client.websocket.boundary.kafka.kafkaproducer.SendToChatServiceProducer;
import dd.mud.client.websocket.boundary.model.websocket.ChatMessage;
import dd.mud.client.websocket.controller.players.PlayerWaitingStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

class ChatMessageForwardingServiceTest extends AbstractClientWebsocketTest {

    @Mock
    private PlayerWaitingStatus playerWaitingStatus;

    @Mock
    private SendToChatServiceProducer sendToChatServiceProducer;

    @InjectMocks
    private ChatMessageForwardingService chatMessageForwardingService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void forwardChatMessagePlayerAllowed() {
        doReturn(Boolean.TRUE).when(playerWaitingStatus).checkAndLock(anyLong());
        doNothing().when(sendToChatServiceProducer).sendToChatService(any());

        chatMessageForwardingService.forwardChatMessage(1L, 2L, getChatMessage());

        verify(playerWaitingStatus, times(1)).checkAndLock(anyLong());
        verify(sendToChatServiceProducer, times(1)).sendToChatService(any());
        verifyNoMoreInteractions(playerWaitingStatus, sendToChatServiceProducer);
    }

    @Test
    public void forwardChatMessagePlayerNotAllowed() {
        doReturn(Boolean.FALSE).when(playerWaitingStatus).checkAndLock(anyLong());
        doNothing().when(sendToChatServiceProducer).sendToChatService(any());

        chatMessageForwardingService.forwardChatMessage(1L, 2L, getChatMessage());

        verify(playerWaitingStatus, times(1)).checkAndLock(anyLong());
        verify(sendToChatServiceProducer, times(0)).sendToChatService(any());
        verifyNoMoreInteractions(playerWaitingStatus, sendToChatServiceProducer);
    }
}