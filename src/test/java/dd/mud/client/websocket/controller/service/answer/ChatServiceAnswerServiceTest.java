package dd.mud.client.websocket.controller.service.answer;

import dd.mud.client.websocket.AbstractClientWebsocketTest;
import dd.mud.client.websocket.boundary.websocket.sender.ChatPlayerTopicSender;
import dd.mud.client.websocket.controller.players.PlayerWaitingStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
class ChatServiceAnswerServiceTest extends AbstractClientWebsocketTest {

    @Mock
    private PlayerWaitingStatus playerWaitingStatus;

    @Mock
    private ChatPlayerTopicSender chatPlayerTopicSender;

    @InjectMocks
    private ChatServiceAnswerService chatServiceAnswerService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void processChatServiceAnswer() {
        // prepare
        doNothing().when(chatPlayerTopicSender).sendToPlayerChatWebsocketTopic(anyLong(), any());
        doNothing().when(playerWaitingStatus).changeStatusOfPlayer(anyLong(), anyBoolean());

        // execute
        chatServiceAnswerService.processChatServiceAnswer(getChatServiceAnswer());

        // test
        verify(chatPlayerTopicSender, times(7)).sendToPlayerChatWebsocketTopic(anyLong(), any());
        verify(playerWaitingStatus, times(1)).changeStatusOfPlayer(anyLong(), anyBoolean());
        verifyNoMoreInteractions(chatPlayerTopicSender, playerWaitingStatus);
    }
}